Rails.application.routes.draw do
  get 'conversations/index'
  get 'conversations/show/:id', to: 'conversations#show', as: 'show_conversation' 
  get '/welcome/privacy_policy', to: 'welcome#privacy_policy', as: 'privacy_policy'
  get 'welcome/terms_of_service', to: 'welcome#terms_of_service', as: 'terms_of_service'
  resources :microposts, only: [:show, :create, :destroy]
  get 'microposts/show_more/:id', to: 'microposts#show_more', as: 'show_more_microposts'
  patch 'microposts/edit', to: 'microposts#edit', as: 'micropost_edit'
  patch 'microposts/:id/toggle_privacy', to: 'microposts#privacy', as: 'micropost_privacy'
  post 'like_comment/:id', to: 'likes#toggle_comment', as: 'toggle_comment_like'
  post 'like_micropost/:id', to: 'likes#toggle_micropost', as: 'toggle_micropost_like'

  namespace :users do 
    get 'profile/:id', to: 'profiles#show', as: 'profile'
    post 'change_avatar', to: 'avatars#create', as: 'create_avatar'
    get 'avatar', to: 'avatars#new', as: 'get_avatar_form'
  end

  resources :comments
  get 'users/profile/:id/albums', to: 'albums#index', as: 'user_albums'
  get 'users/profile/:user_id/albums/:id', to: 'albums#show', as: 'show_album'
  post 'albums', to: 'albums#create', as: 'create_album'
  delete 'album/:id', to: 'albums#destroy', as: 'destroy_album'
  post 'pictures', to: 'pictures#create', as: 'upload_pictures'
  patch 'pictures/:id', to: 'pictures#edit', as: 'edit_picture_caption'
  delete 'pictures/:id', to: 'pictures#destroy', as: 'destroy_picture'
  get 'search_user', to: 'searches#search_user'
  get 'short_search', to: 'searches#short_search'
  get 'conversation/load_more_messages', to: 'conversations#more_messages'


  devise_for :users, skip:[:sessions, :registrations], controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks'
  }
   devise_scope :user do
     get 'users/sign_in', to: 'users/sessions_or_registrations#new', as: 'new_user_session'
     post '/users/sign_in', to: 'users/sessions#create', as: 'user_session'
     delete 'users/sign_out', to: 'users/sessions#destroy', as: 'destroy_user_session'
     patch 'users', to: 'users/registrations#update'
     put 'users', to: 'users/registrations#update'
     delete 'users', to: 'users/registrations#destroy'
     get 'users/sign_up', to: 'users/sessions_or_registrations#new', as: 'new_user_registration'
     get 'users/sign_up', to: 'users/sessions_or_registrations#new', as: 'new_registration'
     post 'users', to: 'users/registrations#create', as: 'user_registration'

   end

  root 'users/sessions_or_registrations#new'
  
  mount ActionCable.server => '/cable'
end

# Example  Date::DATE_FORMATS[:message_long].call date_str
Date::DATE_FORMATS[:message_long] = ->(date) { date.strftime("%B #{date.day.ordinalize}, %Y") }
Date::DATE_FORMATS[:micropost_date] = ->(date) { date.strftime("%B #{date.day.ordinalize} %Y, %l.%M %p") }

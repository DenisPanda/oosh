class AddAlbumReferenceToPictureTable < ActiveRecord::Migration[5.0]
  def change
    add_column :pictures, :album_id, :integer
  end
end

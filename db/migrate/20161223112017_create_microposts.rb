class CreateMicroposts < ActiveRecord::Migration[5.0]
  def change
    create_table :microposts do |t|
      t.text :context
      t.references :user, foreign_key: true
      t.boolean :private, default: false

      t.timestamps
    end
  end
end

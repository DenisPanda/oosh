class AddTimestampsToPersonalMessages < ActiveRecord::Migration[5.0]
  def change
    add_column :personal_messages, :created_at, :datetime
    add_column :personal_messages, :updated_at, :datetime
  end
end

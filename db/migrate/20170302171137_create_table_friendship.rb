class CreateTableFriendship < ActiveRecord::Migration[5.0]
  def change
    create_table :table_friendships do |t|
      t.references :sender, foreign_key: { to_table: :users }, index: true
      t.references :receiver, foreign_key: { to_table: :users }, index: true
      t.string :status, null: false
      t.timestamps
    end
  end
end

class AddPostLocationToMicroposts < ActiveRecord::Migration[5.0]
  def change
    add_column :microposts, :post_location, :integer
  end
end

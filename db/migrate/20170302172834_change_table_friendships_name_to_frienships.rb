class ChangeTableFriendshipsNameToFrienships < ActiveRecord::Migration[5.0]
  def change
    rename_table :table_friendships, :friendships
  end
end

class ChangeMicropostsTable < ActiveRecord::Migration[5.0]
  def change
    rename_column :microposts, :user_id, :author_id
    rename_column :microposts, :post_location, :receiver_id
    remove_index :microposts, name:  "index_conversations_on_author_id"
    remove_index :microposts, name: "index_conversations_on_receiver_id"
  end
end

class AddPictureReferenceToAlbum < ActiveRecord::Migration[5.0]
  def change
    add_column :albums, :picture_id, :integer
  end
end

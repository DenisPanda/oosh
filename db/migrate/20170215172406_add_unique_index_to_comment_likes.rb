class AddUniqueIndexToCommentLikes < ActiveRecord::Migration[5.0]
  def change
    add_index :comment_likes, [:user_id, :comment_id], unique: true
  end
end

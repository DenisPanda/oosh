class CreateCommentLikes < ActiveRecord::Migration[5.0]
  def change
    create_table :comment_likes do |t|
      t.references :comment, foreign_key: true
      t.references :user, foreign_key: true
      t.boolean :like, default: true
    end
  end
end

class CreateMicropostLikes < ActiveRecord::Migration[5.0]
  def change
    create_table :micropost_likes do |t|
      t.references :micropost, foreign_key: true
      t.references :user, foreign_key: true
      t.boolean :like, default: true
    end
  end
end

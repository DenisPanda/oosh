class PicturesController < ApplicationController
  def new
  end

  def create
    picture = Picture.new(image: upload_params['image'], album_id: album_id_params, user_id: current_user.id)

    if picture.save!
  	  render json: { message: "success" }, :status => 200
  	else
  	  render json: { error: picture.errors }, :status => 400
    end
  end

  def edit
    picture = Picture.find_by(user_id: current_user.id, id: id_params)
    picture.assign_attributes(:caption => caption_params[:caption])
    respond_to do |format|
      if current_user.id == picture.user_id && picture.save!
        format.html { render html: id_params, status: :ok }
      else
        format.html { render html: picture.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    picture = Picture.find_by(id: id_params, user_id: current_user.id)

    respond_to do |format|
      if !picture.blank?
        picture.destroy
        format.html { head :ok }
      else
        format.html { render html: picture.errors, status: :unauthorized }
      end
    end
  end

  private
    def upload_params
      params.require('upload').permit('image')
    end

    def album_id_params
      params.require('album_id').to_i
    end

    def caption_params
      params.require('picture').permit('caption')
    end

    def id_params
      params.require(:id)
    end
end

class AlbumsController < ApplicationController 

  def index
    @albums = Album.where(user_id: safe_id_params)
    get_pictures
    render 'albums/index'
  end

  def show
    @pictures = Picture.where(album_id: safe_id_params).paginate(page: params[:page], per_page: 16)
    @album = { name: get_album_name, id: safe_id_params }
    render 'albums/show'
  end

  def create
    album = Album.new(user_id: current_user.id, name: safe_params[:name])
    
    respond_to do |format|
      if album.save!
        format.html { render partial: 'albums/album', locals: { album: album }}
      else
        format.html { render album.errors }
      end
    end
  end

  def destroy
    album = Album.find_by(id: safe_id_params, user_id: current_user.id)

    respond_to do |format|
      unless album.blank?
        album.destroy
        format.html { head :ok }
      else
        format.html { render html: album.errors, status: :unauthorized }
      end
    end
  end

  private


    def safe_params
      params.require(:album).permit(:name)
    end

    def get_album_name
      params.require("album_name")
    end

    def safe_id_params
      params.require(:id)
    end

    def get_pictures
      @album_pictures = {}
      @albums.each do |album|
        pictures = album.pictures.take(4).map do |picture|
          picture.image.url(:medium)
        end
        @album_pictures["#{album.id}"] = (pictures ? pictures : [])
      end
      @album_pictures
    end

end

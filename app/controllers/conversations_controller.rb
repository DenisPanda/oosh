class ConversationsController < ApplicationController

  def index
    @conversations = Conversation.participating(current_user).order(updated_at: :desc)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    conversation = current_user.find_conversation(params[:id])
    @messages = []

    if conversation.blank?
      conversation = Conversation.create!(author_id: current_user.id, receiver_id: params[:id])
      @messages = ''
    else
      @messages = conversation.personal_messages.take(10).reverse
    end

    user = set_user(conversation)
    @messages = insert_dates
    @conversation_id = conversation.id

    respond_to do |format|
      format.html { render partial: 'chat/chat_box', locals: { user: user },  status: :ok }
    end
  end

  def more_messages
    conversation = Conversation.participating_in_conversation(current_user, params['conversation-id'])

    unless conversation.empty?
      conversation = conversation.first
      @messages = next_ten_messages(conversation.id, params['message-id'])
      # If there is more messages then the first one already displayed in the conversation
      if @messages.length > 1
        html = ''
        user = set_user conversation
        @messages = insert_dates
       # @messages.each do |message|
       #   html = html + render_message(message, user)
       # end

        respond_to do |format|
          format.html { render partial: 'personal_messages/message_date_control_flow', locals: { user: user, current_user: current_user }, status: :ok }
        end
      else
        respond_to do |format|
          format.html { head :ok }
        end
      end
    else
      respond_to do |format|
        format.html { head :unauthorized }
      end
    end
  end

  private

    def render_message(message, user)
      if message.class == String
        #ApplicationController.renderer.render partial: 'personal_messages/date', locals: { date: message }
        render_to_string(partial: 'personal_messages/date', locals: { date: message })
      else
        #ApplicationController.renderer.render partial: 'personal_messages/message', locals: { user: user, message: message, current_user: current_user }
        render_to_string(partial: 'personal_messages/message', locals: { user: user, message: message, current_user: current_user })
      end
    end

    def next_ten_messages(con_id, msg_id)
      # Loading also the message already displayed in conversation so the dates won't be messed up
      PersonalMessage.where("conversation_id = ? AND id <= ?", con_id, msg_id).take(11).reverse 
    end

    def insert_dates
      if @messages.blank?
        @messages
      else
        old_date = ''
        new_array = []
        @messages.each do |message|
          date = simple_date message
          if date != old_date
            old_date = date
            new_array << message_date(message) << message
          else
            new_array << message
          end
        end
        new_array
      end
    end

    def set_user(conv)
      id = conversations_not_current_user(conv)
      User.find_by(id: id)
    end

    def conversations_not_current_user(conv)
      current_user.id == conv.author_id ? conv.receiver_id : conv.author_id
    end

    def simple_date(msg)
      msg.created_at.strftime '%d%m%y'
    end

    def message_date(msg)
      Date::DATE_FORMATS[:message_long].call msg.created_at
    end
end

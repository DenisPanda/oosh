class CommentsController < ApplicationController
  before_action :find_user, only: :create
  before_action :correct_user, only: :destroy
  before_action :no_likes, only: :create

  def create
    @comment = Comment.new(safe_params)
    respond_to do |format|
      if @comment.save
        format.html { render partial: 'comments/comment', locals: {comment: @comment}, status: :created }
        format.js {}
      else
        format.html { render @comment.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  def destroy
    respond_to do |format|
      if !@comment.blank? && @comment.destroy
        format.html { head :ok }
        format.js {}
      else
        format.html { head :unprocessable_entity }
        format.js {}
      end
    end
  end

  private
    def safe_params
      params['comment']['user_id'] = current_user.id
      params.require(:comment).permit(:micropost_id, :context, :user_id)
    end

    def no_likes
      @comment_likes = false
    end

    def correct_user
      @comment = Comment.where('user_id=?', "#{current_user.id}").find_by(id: params[:id])
    end

    def find_user
      @users = [] << current_user
    end
end

class MicropostsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :show_more
  before_action :correct_user, only: :destroy

  
  def create
    @micropost = Micropost.new(safe_params)

    respond_to do |format|
      if @micropost.save
        @users = find_micropost_participants
        format.html { render partial: 'microposts/micropost', locals: {micropost: @micropost}, status: :created }
        format.js {}
      else
        format.html { render html: @micropost.errors, status: :unproccessable_entity }
      end
    end
  end

  def edit
    micropost = correct_user_for_edit

    respond_to do |format|
      if micropost && micropost.update({ context: safe_edit_params[:context] })
        format.html { head :accepted }
      else
        format.html { head :unauthorized }
      end
    end
  end

  def destroy
    respond_to do |format|
      if !@micropost.blank? && @micropost.destroy
        format.html { head :ok}
      else
        format.html { head :unproccessable_entity }
      end
    end
  end

  def privacy
    current_user
    micropost = Micropost.find_by(id: params[:id])
    micropost.toggle(:private) if current_user.id == micropost.author_id || current_user.id == micropost.receiver_id
    respond_to do |format|
      if micropost.save
        format.html { head :ok }
        format.js
      else
        format.html { head :unprocessable_entity }
        format.js
      end
    end
  end

  def show_more
    @microposts = current_user.microposts.where('id<?', params[:id]).includes(:comments).take(10)
    @users = User.select(:id, :name).where(id: user_ids(@microposts))

    respond_to do |format|
      if @microposts && @users
        format.html { render partial: 'microposts/micropost', collection: @microposts, status: :ok }
      else
        format.html { @microposts.errors }
      end
    end
  end

  private

    def correct_user
      @micropost = Micropost.where('author_id=? OR receiver_id=?', "#{current_user.id}", "#{current_user.id}").find_by(id: params[:id])
    end

    def correct_user_for_edit
      @micropost = Micropost.where('author_id=?', "#{current_user.id}").find_by(id: safe_edit_params[:id])
    end


    def find_micropost_participants
      User.where(id: [@micropost.author_id, @micropost.receiver_id])
    end

    def safe_params
      params.require(:micropost).permit(:context, :author_id, :receiver_id)
    end

    def safe_edit_params
      params.require(:micropost).permit(:context, :id)
    end
    
end

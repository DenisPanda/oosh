class WelcomeController < Users::SessionsController
  skip_before_action :authenticate_user!

   def terms_of_service
    render 'terms_of_service'
  end

  def privacy_policy
    render 'privacy_policy'
  end
 end

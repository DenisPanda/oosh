class LikesController < ApplicationController
  skip_before_action :verify_authenticity_token

  def toggle_micropost
    exists = true
    like = MicropostLike.where(user_id: current_user.id, micropost_id: params[:id]).first_or_create { exists = false }

    like.destroy if exists

    respond_to do |format|
      format.html { head :ok }
    end
  end

  def toggle_comment
    exists = true
    like = CommentLike.where(user_id: current_user.id, comment_id: params[:id]).first_or_create { exists = false }

    like.destroy if exists

    respond_to do |format|
      format.html { head :ok }
    end
  end

end

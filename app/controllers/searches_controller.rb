class SearchesController < ApplicationController
  def search_user
    @users = User.search(safe_query, current_user.id).paginate(page: params[:page], per_page: 20)
    @search = safe_query
    render 'users/search/search'
  end

  def short_search
    users = User.search(safe_query, current_user.id).take(6)

    respond_to do |format|
      format.html { render partial: 'layouts/short_search', locals: { users: users }, status: :ok }
    end
  end

  private
    def safe_query
      params.require("search-query")
    end
end

class Users::ProfilesController < ApplicationController
  helper ApplicationHelper
  attr_accessor :friend, :microposts

  def show
    @user = User.find_by(id: params[:id])
    #@friend = Friendship.check(@user.id, current_user.id)
   # is_friend_blank?(@friend)
   # fetch_microposts(@friend)
    @microposts = Micropost.where(receiver_id: @user.id).includes(:comments).take(10)
    post_ids = get_micropost_ids @microposts
    @micropost_likes = MicropostLike.where(micropost_id: post_ids[:micropost_ids], user_id: current_user.id).pluck(:micropost_id)
    @comment_likes = CommentLike.where(comment_id: post_ids[:comment_ids], user_id: current_user.id).select(:comment_id).pluck(:comment_id)
    @users = User.where(id: all_user_ids)
    render 'users/profile/show'
  end


  private 

    def get_micropost_ids object
      m_arr = []
      c_arr = []
      object.each do |m|
        m_arr << m.id
        m.comments.each do |c|
          c_arr << c.id
        end
      end
      { micropost_ids: m_arr, comment_ids: c_arr.compact }
    end

    def all_user_ids
      ids = []
      @microposts.each do |micropost| 
        ids << micropost.author_id << micropost.receiver_id
      end
      ids
    end

    def is_friend_blank?(friend)
      if friend.blank?
        @friend = friend.new
        @friend.status = ''
      end
    end

    def fetch_microposts(friend)
      if (friend.status == 'accepted' || @user.id == current_user.id)
        @microposts = @user.microposts.includes(:comments).take(10)
      else
        @microposts = Micropost.where(receiver_id: @user.id, private: true).includes(:comments).take(10)
      end
    end
end

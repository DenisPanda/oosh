class Users::AvatarsController < ApplicationController
  def new
    respond_to do |format|
      format.html { render partial: 'avatars/avatar_form', status: :ok }
    end
  end

  def create
    redirect_to users_profile_path(current_user.id) if current_user.update_attribute(:avatar, avatar_params[:image])
  end

  private
    def avatar_params
      params.require(:avatar).permit(:image)
    end
end

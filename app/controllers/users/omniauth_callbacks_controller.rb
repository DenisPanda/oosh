class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  # You should configure your model like this:
  # devise :omniauthable, omniauth_providers: [:twitter]

  # You should also create an action method in this controller like this:
  def self.provides_callback_for(provider, sn_name)
    class_eval %Q{
      def #{provider}
        @user = User.from_omniauth(request.env["omniauth.auth"], "#{sn_name}")

         if @user.persisted?
           sign_in @user, :event => :authentication #this will throw if @user is not activated
           redirect_to root_path
           set_flash_message(:notice, :success, :kind => "#{sn_name}") if is_navigational_format?
         else
           session["devise.#{provider}_data"] = request.env["omniauth.auth"]
           redirect_to new_user_registration_url
         end
      end
    }
  end

  { :twitter => "Twitter", :facebook => "Facebook", :google_oauth2 => "Google+"}.each do |provider, sn_name|
    provides_callback_for provider, sn_name
  end


  # More info at:
  # https://github.com/plataformatec/devise#omniauth

  # GET|POST /resource/auth/twitter
  # def passthru
  #   super
  # end

  # GET|POST /users/auth/twitter/callback
  def failure
    render :plain => env['omniauth.error'].inspect
  end

  # protected

  # The path used when OmniAuth fails
  # def after_omniauth_failure_path_for(scope)
  #   super(scope)
  # end
end

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  
  private
    def user_ids(posts) # Make an array populated with all users which will be shown on the page
      s = Set.new
      posts.each { |post| s << post.author_id << post.receiver_id }
      posts.each do |post|
        s << post.author_id << post.receiver_id
        if post.comments.any?
          post.comments.each do |comment|
            s << comment.user_id
          end
        end
      end
      s.to_a
    end

end

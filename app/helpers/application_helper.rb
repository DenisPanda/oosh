module ApplicationHelper

  def online_status(user)
    content_tag :span, user.name,
      class: "user-#{user.id} online_status #{'online' if user.online?}"
  end

  def album_pics(album, i)
    if @album_pictures
      @album_pictures["#{album.id}"][i]
    else
      false
    end
  end

  def page_title(title)
    title.blank? ? 'Oosh' : "#{title} | Oosh" 
  end

  def user_thumb(obj)
    if current_user.id == object_user_id(obj)
      current_user.avatar.url(:thumb)
    else
      image = @users.map do |user|
        # object_user_id first checks obj class then decides whic attribute to access
        user.avatar.url(:thumb) if object_user_id(obj) == user.id
      end
      image.compact.first
    end
  end

  def thumb(obj)
    obj.image.url(:medium)
  end

  def large_image(obj)
    obj.image.url(:large)
  end

  def caption(pic)
    "#{pic.caption}" unless pic.blank?
  end

  def author_of_message(id, user, current_user)
    user.id == id ? user.name : current_user.name
  end

  def chat_image(id, user, current_user)
    user.id == id ? user.avatar.url(:thumb) : current_user.avatar.url(:thumb)
  end

  def message_time(date)
    date.strftime('%l.%M %p')
  end

  def current_user_is_author?(message, current_user)
    current_user.id == message.user_id 
  end


  private 

    def object_user_id(obj)
      if obj.class == Micropost
        obj.author_id
      else
        obj.user_id
      end
    end

    def user_by_id(id)
      if current_user.id == id.to_i
        current_user.name
      else
        @users.find { |user| user[:id].to_i == id.to_i }.name
      end
    end

    def profile_link(id, klass = '')
      name = user_by_id(id)
      link_to(name, users_profile_path(id.to_i), class: "link-no-style #{klass}")
    end

    def date_and_time(object_record)
      date = Date::DATE_FORMATS[:micropost_date].call object_record.created_at
    end

    def micropost_sender_receiver_html(micropost)
      receiver = micropost.receiver_id
      author = micropost.author_id
      safe_join([ profile_link(author), "<span class='glyphicon glyphicon-chevron-right'></span>".html_safe, profile_link(receiver) ])
    end

    def friend_icon_active?(friend)
      (friend.status == 'accepted' || friend.status == 'pending') ? 'active' : nil
    end

end

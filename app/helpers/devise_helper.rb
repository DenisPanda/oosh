module DeviseHelper
  def devise_error_messages!
    return '' if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    html = <<-HTML
    <div class='row row-messages'>
      <div class='col-md-12 col-lg-12 col-xs-12 col-sm-12 alert alert-danger text-center'>
        #{messages}
      </div>
    </div>
    HTML

    html.html_safe
  end
end

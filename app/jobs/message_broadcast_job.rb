class MessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message, conversation)
    ActionCable.server.broadcast("conversations_#{conversation.id}_channel", message: render_message(message, conversation), conversation_id: "#{conversation.id}")
  end

  private 

    def render_message(message, conversation)
      user = User.find_by(id: message.user_id)
      user_2 = second_user(user, conversation)
      ApplicationController.renderer.render(partial:'personal_messages/message', locals: { message: message, user: user, current_user: user_2 })
    end
    
    def second_user(user, conversation)
      conversation.author_id == user.id ? user : usr(conversation.author_id)
    end

    def usr(id)
      User.find_by(id: id)
    end

end

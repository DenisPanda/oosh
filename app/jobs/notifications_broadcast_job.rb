class NotificationsBroadcastJob < ApplicationJob
  queue_as :default

  def perform(personal_message, conversation)
    message = render_message(personal_message)
    receiver_id = get_receiver_id(personal_message, conversation)
    ActionCable.server.broadcast "notifications_#{receiver_id}_channel",
				message: message,
        conversation_id: conversation.id
  end


  private

    def get_receiver_id msg, conv
      msg.user_id == conv.receiver_id ? conv.author_id : conv.receiver_id
    end

    def render_notification message
      NotificationsController.render partial: 'notifications/notification', locals: { message: message, sender_name: current_user.name }
    end

end

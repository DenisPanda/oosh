class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :micropost
  has_many :comment_likes, dependent: :destroy
end

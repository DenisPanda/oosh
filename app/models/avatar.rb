class Avatar < ApplicationRecord
  belongs_to :user
  has_attached_file :avatar, :styles => { :large => '500x500#', :medium => '300x300#', :thumb => '200x200#' }
  validates_attachment :avatar, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png"] }
end

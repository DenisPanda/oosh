class Album < ApplicationRecord
  belongs_to :user
  has_many :pictures, class_name: 'Picture', foreign_key: 'album_id', dependent: :destroy

end

class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:twitter, :facebook, :google_oauth2]

  validates :name, presence: true
  has_many :personal_messages, dependent: :destroy
  has_many :authored_conversations, class_name: 'Conversation', foreign_key: :author_id
  has_many :received_conversations, class_name: 'Conversation', foreign_key: :receiver_id
  has_many :microposts, foreign_key: :author_id, dependent: :destroy
  has_many :microposts, foreign_key: :receiver_id, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :micropost_likes, dependent: :destroy
  has_attached_file :avatar, :styles => { :large => '500x500#', :medium => '300x300#', :thumb => '200x200#' }, default_url: "/images/missing/avatar/:style/missing.png"
  validates_attachment :avatar, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png"] }
  has_many :albums, dependent: :destroy
  has_many :pictures, class_name: 'Picture', foreign_key: 'user_id', dependent: :destroy
  has_many :sent_firendships, class_name: 'Friendship', foreign_key: :sender_id, dependent: :destroy
  has_many :received_firendships, class_name: 'Friendship', foreign_key: :receiver_id, dependent: :destroy

  def self.search(name, excluded_id)
    if Rails.env == 'production'
      where('name ~* ?', "^#{name}").where.not(id: excluded_id)
    else
      where("name REGEXP ?", name).where.not(id: excluded_id)
    end
  end

  def online?
    !Redis.new.get("user_#{self.id}_online").nil?
  end

   def find_conversation(user_id)
    self.authored_conversations.or(self.received_conversations).find_by('author_id=? OR receiver_id=?', user_id, user_id)
  end


  def self.from_omniauth(auth, sn_name)
    where(email: auth.info.email).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name   
      user.provider = sn_name
    end
  end

end

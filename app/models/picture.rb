class Picture < ApplicationRecord
  has_attached_file :image, styles: { medium: "300x300#", large: "1280x720>" }, default_url: "/images/:style/missing.png"
  belongs_to :album
  belongs_to :user
  default_scope -> { order(created_at: :desc) }

  validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png"] }

  def self.search(id, current_page)
    where(id: id).paginate(page: current_page, per_page: 16)
  end
end

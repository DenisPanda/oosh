class PersonalMessage < ApplicationRecord
  belongs_to :user
  belongs_to :conversation
  validates :body, presence: true
  default_scope -> { order(created_at: :desc) }

  after_create_commit do
    conversation = self.conversation
    conversation.touch
    MessageBroadcastJob.perform_later(self, conversation)
  end

  def receiver
    con = conversation
    user_id == con.author_id ? con.receiver : con.author
  end

end

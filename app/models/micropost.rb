class Micropost < ApplicationRecord
  belongs_to :author, class_name: 'User'
  belongs_to :receiver, class_name: 'User'
  has_many :comments, dependent: :destroy
  validates :context, presence: true, length: { minimum: 1, maximum: 300 }
  default_scope -> { order(created_at: :desc) }
  has_many :micropost_likes, dependent: :destroy


end

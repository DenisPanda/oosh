class Friendship < ApplicationRecord
  belongs_to :sender, class_name: 'User'
  belongs_to :receiver, class_name: 'User'

  def self.check(a,b)
    where(sender_id: a, receiver_id: b).or(Friendship.where(sender_id: b, receiver_id: a))
  end


end

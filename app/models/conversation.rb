class Conversation < ApplicationRecord
  belongs_to :author, class_name: 'User'
  belongs_to :receiver, class_name: 'User'
  has_many :personal_messages
  validates :author, uniqueness: { scope: :receiver }

  scope :participating, -> (user) { where("author_id = ? OR receiver_id = ?", user.id, user.id) }

  def participates?(user_id)
    user_id = user_id.to_i
    author_id == user_id || receiver_id == user_id
  end

  def self.participating_in_conversation(user, id)
    where(id: id, author_id: user.id).or(where(id: id, receiver_id: user.id))
  end
end

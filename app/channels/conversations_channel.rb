class ConversationsChannel < ApplicationCable::Channel

  def subscribed
    stream_from("conversations_#{params['conversation_id']}_channel")
  end

  def unsubscribed
  end

  def send_message(data)
    conversation = Conversation.find_by(id: data['conversation_id'])
   # conversation = current_user.find_conversation(data['conversation_id']).first_or_create! do |conversation|
   #   conversation.receiver_id = data['user_id']
   # end

    if conversation.participates?(current_user.id) && conversation.participates?(data['user_id'])
      personal_message = current_user.personal_messages.build({ 
        body: data['message'],
        conversation_id: conversation.id
      })
      personal_message.save!
    end
  end

  #private

  # First smaller id number then larger, example => 'conversations_11_67_channel
  #  def channel_id
  #    (params[:id].to_i <=> current_user.id) < 1 ? "#{params[:id]}_#{current_user.id}" :  "#{current_user.id}_#{params[:id]}"
  #  end
end

$(document).ready ->
  self = $(@)
  profilePhoto = $('#profile-photo-container')
  userName = profilePhoto.find('.user-profile-name')
  links = profilePhoto.find('.avatar-footer-links')
  profilePhoto.find('.profile-footer-links')
  profilePhoto.hover (->
    userName.hide()
    links.show()
  ),
  ->
    links.hide()
    userName.show()

# Change avatar form AJAX
  (->
    el = {
      body: $(document.body),
      form: [],
      setForm: ->
        @.form = @.body.children('#avatar-form-btn')
    }
    
    profilePhoto.on('ajax:beforeSend', '#get-avatar-form', (e, xhr)->
     if(el.form.length > 0)
        xhr.abort()
        el.form.trigger('click')
    )
    
    profilePhoto.on('ajax:success', '#get-avatar-form', (e, data, status, xhr)->
      el.body.append(data)
      el.setForm() if el.form.length < 1
      el.form.trigger('click')
    ).on('ajax:error', '#get-avatar-form', (e, xhr, status, error)->
      alert 'error je: '+error
    )
  )()

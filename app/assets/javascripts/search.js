$(document).ready(function(){
  var searchForm = $('.navbar-form'),
    searchField = searchForm.find('#search-field'),
    timeout = null,
    results = searchForm.find('#query-results'),
    oldVal = null;

  searchField.on('keydown', function(e){
    clearTimeout(timeout);
    if(e.which === 13){
      $('#submit-search').trigger('click');
    }

    timeout = setTimeout(function(){
      $.ajax({
        url: '/short_search',
        type: 'GET',
        data: { 'search-query': searchField.val() },
        datatype: 'html',
        beforeSend: function(xhr) {
          if(oldVal === searchField.val() || searchField.val().length < 1){
            xhr.abort(); }
          else {
            oldVal = searchField.val();
          }
        }
      })
      .done(function(data){
         results.find('ul').remove()
         results
         .append(data)
         .slideDown(200);
      });
    }, 700);
  });

  $(document).mouseup(function (e)
  {
      var container = searchForm;

      if (!container.is(e.target) // if the target of the click isn't the container...
          && container.has(e.target).length === 0) // ... nor a descendant of the container
      {
          results.hide();
      }
  });
})

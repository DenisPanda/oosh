$(document).ready(function(){
  Dropzone.autoDiscover = false;

  $('#picture-upload').dropzone({
    maxFilesize: 3,
    paramName: 'upload[image]',
    addRemoveLinks: true,
    parallelUploads: 1,
    uploadMultiple: false,
    autoProcessQueue: true,
    init: function(){
      this.on("sending", function(file, xhr, formData){
        formData.append('album_id', $('#album-id').html());
      })
      .on("complete", function (file) {
        if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
          window.location.reload();
        }
      })
    },
    success: function(file, response){
		 	$(file.previewTemplate).find('.dz-remove').attr('id', response.fileID);
		 	$(file.previewElement).addClass("dz-success");
    }
  });
})

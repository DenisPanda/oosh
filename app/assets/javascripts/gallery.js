$(document).ready(function () {
  var albumCollection = $('#album-collection');

  $('#newAlbumModal').on('shown.bs.modal', function () {
    $('#album_name').focus();
  });

  $('#create-album-form').on('ajax:success', function (e, data, status, xhr) {
    var self = $(this);
    $('.no-content').hide();
    albumCollection.append(data);
    self.find('.close-form').trigger('click');
    self.find('#album_name').val('');
  }).on('ajax:error', function (e, xhr, status, error) {
    alert('error');
  });

  albumCollection.on('ajax:success', '.delete-album', function (e, xhr, status, error) {
    var _this = this;

    var albumContainer = $(this).closest('.album-container');
    albumContainer.fadeOut(500, function () {
      _this.remove();
    });
  }).on('ajax:error', '.album-collection', function (e, xhr, status, error) {
    alert(error);
  });

  // Initialize tooltip if there is a gallery-grid div
  var galleryDiv = $('#gallery-grid');
  if (galleryDiv.length > 0) {
    galleryDiv.tooltip({
      selector: "[data-toggle='tooltip']",
      trigger: 'hover'
    });
  }

  var
  // ACTIVITY INDICATOR

  activityIndicatorOn = function activityIndicatorOn() {
    $('<div id="imagelightbox-loading"><div></div></div>').appendTo('body');
  },
      activityIndicatorOff = function activityIndicatorOff() {
    $('#imagelightbox-loading').remove();
  },


  // OVERLAY

  overlayOn = function overlayOn() {
    $('<div id="imagelightbox-overlay"></div>').appendTo('body');
  },
      overlayOff = function overlayOff() {
    $('#imagelightbox-overlay').remove();
  },


  // CLOSE BUTTON

  closeButtonOn = function closeButtonOn(instance) {
    $('<button type="button" id="imagelightbox-close" title="Close"></button>').appendTo('body').on('click touchend', function () {
      $(this).remove();instance.quitImageLightbox();return false;
    });
  },
      closeButtonOff = function closeButtonOff() {
    $('#imagelightbox-close').remove();
  },


  // CAPTION

  captionOn = function captionOn() {
    var description = $('a[href="' + $('#imagelightbox').attr('src') + '"] img').attr('alt');
    if (description.length > 0) $('<div id="imagelightbox-caption">' + description + '</div>').appendTo('body');
  },
      captionOff = function captionOff() {
    $('#imagelightbox-caption').remove();
  },


  // NAVIGATION

  navigationOn = function navigationOn(instance, selector) {
    var images = $(selector);
    if (images.length) {
      var nav = $('<div id="imagelightbox-nav"></div>');
      for (var i = 0; i < images.length; i++) {
        nav.append('<button type="button"></button>');
      }nav.appendTo('body');
      nav.on('click touchend', function () {
        return false;
      });

      var navItems = nav.find('button');
      navItems.on('click touchend', function () {
        var $this = $(this);
        if (images.eq($this.index()).attr('href') != $('#imagelightbox').attr('src')) instance.switchImageLightbox($this.index());

        navItems.removeClass('active');
        navItems.eq($this.index()).addClass('active');

        return false;
      }).on('touchend', function () {
        return false;
      });
    }
  },
      navigationUpdate = function navigationUpdate(selector) {
    var items = $('#imagelightbox-nav button');
    items.removeClass('active');
    items.eq($(selector).filter('[href="' + $('#imagelightbox').attr('src') + '"]').index(selector)).addClass('active');
  },
      navigationOff = function navigationOff() {
    $('#imagelightbox-nav').remove();
  },


  // ARROWS

  arrowsOn = function arrowsOn(instance, selector) {
    var $arrows = $('<button type="button" class="imagelightbox-arrow imagelightbox-arrow-left"></button><button type="button" class="imagelightbox-arrow imagelightbox-arrow-right"></button>');

    $arrows.appendTo('body');

    $arrows.on('click touchend', function (e) {
      e.preventDefault();

      var $this = $(this),
          $target = $(selector + '[href="' + $('#imagelightbox').attr('src') + '"]'),
          index = $target.index(selector);

      if ($this.hasClass('imagelightbox-arrow-left')) {
        index = index - 1;
        if (!$(selector).eq(index).length) index = $(selector).length;
      } else {
        index = index + 1;
        if (!$(selector).eq(index).length) index = 0;
      }

      instance.switchImageLightbox(index);
      return false;
    });
  },
      arrowsOff = function arrowsOff() {
    $('.imagelightbox-arrow').remove();
  };

  // Initialize gallery

  var gallery = galleryDiv.find('a[data-image-lightbox="e"]').imageLightbox({
    onStart: function onStart() {
      overlayOn(), closeButtonOn(gallery);
    },
    onLoadStart: function onLoadStart() {
      captionOff();activityIndicatorOn();
    },
    onLoadEnd: function onLoadEnd() {
      captionOn();activityIndicatorOff();
    },
    onEnd: function onEnd() {
      overlayOff(), captionOff(), closeButtonOff(), activityIndicatorOff();
    }
  });

  var menu,
      el,
      oldMenu,
      count = 0;
  var allMenus = galleryDiv.find('.picture-options'); // Must be updated if picture AJAX added

  galleryDiv.on('mouseover', 'img, .picture-option-btn', function () {
    count = 0;
    count++;
    var img = $(this);
    var picContainer = img.closest('.picture-container');
    menu = img.parent('a').siblings('.picture-options');
    menu.show();
  }).on('mouseout', 'img, .picture-option-btn', function () {
    count--;
    setTimeout(function () {
      if (!count) {
        menu.hide();
      }
    }, 50);
  }).on('mouseover', function () {
    setTimeout(function () {
      if (!count) {
        allMenus.hide();
      }
    }, 80);
  }).on('click', '.picture-option-btn', function (e) {
    e.preventDefault();
  });

  // Focus edit caption modal
  $('#editPictureCaption').on('shown.bs.modal', function () {
    $(this).find('#caption-input').focus();
  });

  var editCaptionPath = void 0,
      imageEl = void 0;

  // Edit caption AJAX
  galleryDiv.on('ajax:success', '#edit-caption-form', function (e, data, status, xhr) {
    var caption = galleryDiv.find('#caption-input').val();
    galleryDiv.find('.close-form').trigger('click');
    imageEl.attr('alt', caption);
    $('[data-toggle="tooltip"]').tooltip();
    var parentDiv = imageEl.closest('.picture-container');
    parentDiv.attr('title', caption);
    parentDiv.tooltip('fixTitle');
  }).on('ajax:error', '#edit-caption-form', function (error) {
    alert('Neki error');
  });

  // Delete picture

  galleryDiv.on('click', '.delete-picture-btn', function () {
    var optionsDiv = $(this).parent('div.picture-options'),
        form = galleryDiv.find('#delete-picture-form'),
        deletePicturePath = '/pictures/' + optionsDiv.data('picture-id'),
        imageLink = void 0;
    form.attr('action', deletePicturePath);
    imageEl = optionsDiv.siblings('a').children('img');
  });

  var cleanupImage = function cleanupImage(image) {
    gallery = this.filter(function () {
      return !$(this).is(image);
    });
    this.removeFromImageLightbox(image);
  };

  galleryDiv.on('ajax:success', '#delete-picture-form', function () {
    var imageLink = imageEl.closest('a');
    cleanupImage.call(gallery, imageLink);
    imageEl.closest('.picture-container').fadeOut(800, function () {
      $(this).remove();
    });
    $(this).find('.close-form').trigger('click');
  }).on('click', '.edit-picture-caption-btn', function () {
    var optionsDiv = $(this).parent('div.picture-options'),
        form = galleryDiv.find('#edit-caption-form'),
        inputField = form.find('#caption-input');
    imageEl = optionsDiv.siblings('a').children('img');
    var imageCaption = imageEl.attr('alt');
    editCaptionPath = '/pictures/' + optionsDiv.data('picture-id');
    form.attr('action', editCaptionPath);
    if (imageCaption && imageCaption.length > 0) {
      inputField.val(imageCaption);
    } else {
      inputField.val('');
    };
  });
});

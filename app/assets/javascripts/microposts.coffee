$(document).ready ->
  self = $(@)
# Microposts
  $.ajaxSetup({
    dataType: 'html'
   })

# Show more handler
  show_more_microposts =
    micropostOldId: null
    ,
    findLastPostId: ->
      microposts = $('.micropost-id')
      $(microposts[microposts.length-1]).val()
    ,
    refreshOldId: ->
      @micropostOldId = @findLastPostId()
    ,
    changePath: ->
      el = $('#show-more-microposts')
      r = /(show_more\/)(\d+)/
      link = el.attr('href').replace(r, "show_more/#{@findLastPostId()}")
      el.attr('href', link )
    ,
    isMoreMicroposts: ->
      microposts = $('.micropost-container')
      initial_micropost_count = 11
      if(microposts.length < initial_micropost_count)
        $('.show-more-link').remove()

  show_more_microposts.isMoreMicroposts()

# Show more AJAX listener
  self.on('ajax:success', '#show-more-microposts', (e, data, status, xhr)->
    m = show_more_microposts
    m.refreshOldId()
    $(data).insertAfter($('.micropost-container').last()).hide().slideDown 400
    if m.micropostOldId == m.findLastPostId()
      $('.show-more-link').remove()
    else
      show_more_microposts.changePath()
  ).on('ajax:error', 'show-more-microposts', (e, error, status)->
    alert 'Error: '+error
  )

# Create micropost
  self.on("ajax:success", '#micropost-form', (e, data, status, xhr)->
    textArea = $('.micropost-text-area')
    micropostIcon = $('#form-profile-icon')
    textArea.val('')
    micropostIcon.addClass 'highlight-icon'
    removeClass = () => micropostIcon.removeClass 'highlight-icon'
    $(data).insertAfter($('.micropost-container').first()).hide().slideDown 400
    setTimeout removeClass, 2500
  ).on("ajax:error", '#micropost-form', (e, xhr, status, error) ->
    $(".micropost-div").append "<div class= 'alert alert-danger'>Something is wrong with the form</div><p>Response text: "+error+"</p>"
  )


# Delete micropost
  self.on("ajax:success", 'div.container-fluid .delete-micropost', (e, data, status, xhr) ->
    $(@).closest('.micropost-container').fadeOut 'slow', ()->
      @remove()
  ).on("ajax:error", (e, xhr, status, error) ->
    alert 'Error: ' + error
  )

# Edit micropost
  self.on 'click','.edit-micropost-btn', (e)->
    e.preventDefault()
    editBtn = $(@)
    glyph = editBtn.children('span.glyphicon')
    micropost = editBtn.closest('.micropost-div')
    paragraf = micropost.find('.micropost-paragraf')
    editForm =  micropost.find('.micropost-edit-form')
    submitBtn = editForm.find('.edit-submit-btn')
    textArea = editForm.children('.micropost-text-area')
    text = paragraf.html()
    formControl = {
      showForm: ->
        glyph.addClass('active')
        paragraf.slideUp('fast')
        editForm.slideDown('fast')
        textArea.val(text).focus()
      ,
      hideForm: (data=null)->
        paragraf.html(data) if data
        glyph.removeClass('active')
        editForm.slideUp('fast')
        paragraf.slideDown('fast')
    }

    formControl.showForm()

    editForm.find('.edit-cancel-btn').on('click', ->
      formControl.hideForm()
    )

    editForm.on('ajax:success', (e, data, status, xhr)->
      paragraf.html(textArea.val())
      formControl.hideForm(data)
    ).on('ajax:error', (e, status, xhr, error)->
      alert 'Error: '+ error
    )



# Create comments
  self.on('ajax:success', '.comment-form', (e, data, status, xhr)->
    el = $(@)
    el.children('.comment-text-area').val('')
    commentRow = $(el.closest('.micropost-div').find('.comment-row'))
    commentRow.append(data)
    lastComment = $(commentRow.find('.comment-container:last'))
    lastComment.hide().slideDown('slow')
    
  ).on('ajax:error', '.comment-form', (e, xhr, status, error)->
    alert 'Error: ' + error
  )

# Delete comments
  self.on('ajax:success', '.delete-comment', (e, data, status, xhr)->
    comment = $(@).closest('.comment-container').slideUp(1000).delay(1000).remove()
  ).on('ajax:error', '.delete-comment', (e, xhr, status, error)->
    alert 'Error: ' +error
  )
  

# Show only last 2 comments

  microposts = $('.micropost-container')

  commentsManipulator = {
    allMicroposts: ->
      $('.micropost-container')
    ,

    toggleComments: (microposts, initializeShow = false) ->
      for micropost in microposts
        el = $(micropost)
        comments = el.find('.comment-container')
        size = comments.size()
        @showShowButton(el) if size > 2

        if initializeShow
          @lastTwoElements(comments).fadeIn('fast')
        else
          @exceptLastTwo(comments).fadeToggle('fast')
    ,

    lastTwoElements: (elements) ->
      size = elements.size()
      elements.slice(size-2, size)
    ,

    showShowButton: (element) ->
      element.find('.show-more').css('display', 'block')

    exceptLastTwo: (elements) ->
      size = elements.size()
      elements.slice(0, size-2)
    ,
    
      # First micropost is input form
    filterMicroposts: (microposts) ->
      microposts = microposts.slice 1, microposts.size(),

    initializeShow: ->
      @toggleComments @filterMicroposts(@allMicroposts()), true
  }

  commentsManipulator.initializeShow()

  self.on 'click', '.show-more', (e)->
    e.preventDefault()
    micropost = $(@closest('.micropost-container'))
    el = $(@)
    if el.data('show')
      el.data('show', false).text('Show more')
      comments = micropost.find('.comment-container')
      commentsManipulator.exceptLastTwo(comments).fadeOut('fast')
    else
      el.data('show', true)
      el.text('Show less')
      comments = micropost.find('.comment-container')
      comments.fadeIn('fast')

# Toggle micropost private/public
  self.on('ajax:success', '.privacy-btn', (e, data, status, xhr)->
    el = $(@).children('span.glyphicon')
    if el.hasClass('active') then el.removeClass('active') else el.addClass('active')
  ).on('ajax:error', (e, status, error)->
    alert 'Error: '+ error
  )


# Like micropost
 
  self.on('ajax:success', '.micropost-like-btn, .comment-like-btn', (e, data, xhr, status)->
    self = $(@)
    likeGlyph = self.children('.glyphicon')
    countEl = self.children('.count')
    countNum = parseInt countEl.html()
    if likeGlyph.hasClass('active')
      likeGlyph.removeClass('active')
      countEl.html(--countNum)
    else
      likeGlyph.addClass('active')
      countEl.html(++countNum)
  ).on('ajax:error', '.micropost-like-btn, .comment-like-btn', (data, error, status)->
    alert 'Error: ' + error
  )

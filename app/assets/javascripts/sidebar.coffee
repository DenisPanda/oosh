$(document).ready (e)->
  # Clickable elements
  arrow = $("span.glyphicon-chevron-left")
  handler = (element, firstClass, secondClass) ->

  class ClassToggler
    constructor: (@targetName, @firstClass, @secondClass) ->

    toggle: =>
      el = @target()
      el.toggleClass ()=>
        @checkClass(el)

    checkClass: (element)=>
      if element.hasClass @firstClass then @secondClass else @firstClass

    target: =>
     $(@targetName)

  # Handlers
  arrow.on 'click', (e)->
    e.preventDefault()
    $("#wrapper").toggleClass("sidebar-invisible")
    showHideSidebar = new ClassToggler(
      'span.glyphicon-chevron-left',
      'glyphicon-chevron-left',
      'glyphicon-chevron-right'
    )
    showHideSidebar.toggle()

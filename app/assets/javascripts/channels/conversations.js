$(document).ready(function () {
  var body = $(document.body),
      chatContainer = body.find('.chat-container'),
      message_area = null,
      messages_to_bottom = function messages_to_bottom() {
    $(message_area).scrollTop(message_area.prop("scrollHeight"));
  },

  // Chat existance checker chatExists.check()
  chatExists = {
    exists: false,
    existingObject: {},
    check: function check() {
      this.exists = false;
      var chats = $('.chat-container .popup-box');
      for (var i = 0; i < chats.length; i++) {
        if ($(chats[i]).data('user-id') === userId) {
          this.exists = true;
          this.existingObject = $(chats[i]);
          break;
        }
      }
      return this.exists;
    }
  },


  // Scroll load messages with Throttle
  scrollLoad = {
    addListener: function addListener(conversation) {
      var textArea = conversation.find('.popup-messages'),
          throttle = window.Dash.throttle,
          self = this;

      function updatePosition() {
        self.updatePosition(textArea);
      }

      textArea.on('scroll', throttle(updatePosition, 1000));
    },
    updatePosition: function updatePosition(textArea) {
      if (textArea.scrollTop() < 30) {
        var messageId = textArea.find('.direct-chat-msg').first().data('message-id'),
            conversationId = textArea.closest('.popup-box').data('conversation-id');
        this.addMessages(textArea, conversationId, messageId);
      }
    },
    addMessages: function addMessages(textArea, conversationId, messageId) {
      var self = this;
      $.ajax({
        url: '/conversation/load_more_messages',
        dataType: 'html',
        contentType: 'text/html',
        type: 'GET',
        data: { 'conversation-id': conversationId, 'message-id': messageId },
        beforeSend: function beforeSend(xhr) {
          if (textArea.find('div.loader').length > 0) {
            xhr.abort();
          } else {
            textArea.find('.direct-chat-messages').prepend($("<div class=loader></div>"));
          }
        },
        success: function success(data) {
          if (data) {
            var messagesArea = textArea.find('.direct-chat-messages'),
                scrollHeightBefore = textArea[0].scrollHeight;
            textArea.find('.loader').remove();
            for (var i = 0, divs = messagesArea.children('div'); i < 2; i++) {
              $(divs[i]).remove();
            }
            messagesArea.prepend($(data));
            textArea.scrollTop(textArea[0].scrollHeight - scrollHeightBefore);
          } else {
            self.removeListener(textArea.closest('.popup-box'));
            textArea.find('.loader').remove();
          }
        }
      });
    },
    removeListener: function removeListener(conversation) {
      conversation.find('.popup-messages').unbind('scroll');
    }
  },


  // Session storage handler
  storageHandler = {
    storeConversations: function storeConversations() {
      var conversations = $('.popup-box'),
          storage = window.sessionStorage;
      if (conversations.length > 0) {
        var conLength = conversations.length,
            conClone = {},
            msgLength = null,
            emptyDiv = {},
            _html = '';
        var MAX_MSG = 10;
        for (var i = 0; i < conLength; i++) {
          conClone = $(conversations[i]).clone();
          msgLength = conClone.find('.direct-chat-msg').length;
          if (msgLength > MAX_MSG) {
            conClone.find('.direct-chat-msg').slice(0, msgLength - MAX_MSG).remove();
          }
          emptyDiv = $('<div></div>');
          emptyDiv.append(conClone); // necessary because html() doesn't include the parent element
          _html += emptyDiv.html();
        }
        storage.clear();
        storage.setItem('conversations', _html);
      } else {
        storage.clear();
      };
    },
    check: function check() {
      var storage = window.sessionStorage;
      if (sessionStorage.length > 0) {
        html = $.parseHTML(storage.getItem('conversations'));
        $('.chat-container').append(html);
        this.allToBottom();
        this.reinitializeChats();
      }
    },
    reinitializeChats: function reinitializeChats() {
      var conversations = $('.popup-box');
      for (var i = 0, conversation = {}; i < conversations.length; i++) {
        conversation = $(conversations[i]);
        conversationCableHandler(conversation.data('conversation-id'), conversation.data('user-id'));
        scrollLoad.addListener(conversation);
      }
    },

    allToBottom: function allToBottom() {
      var conversations = $('.popup-box');
      for (var i = 0; i < conversations.length; i++) {
        message_area = $(conversations[i]).find('.popup-messages');
        messages_to_bottom();
      }
    }
  },


  //localStorage
  userId = null;

  // Chat box constants
  var CHAT_BOX_WIDTH = 298,
      CHAT_WIDTH_PLUS_MARGIN = CHAT_BOX_WIDTH + 70,
      CHAT_HEAD_BODY_DIFFERENCE = '-355px';

  // ACTION CABLE HANDLER 
  var conversationCableHandler = function conversationCableHandler(conversationId, userId) {
    App['conversation_' + conversationId] = App.cable.subscriptions.create({
      channel: 'ConversationsChannel',
      conversation_id: conversationId,
      user_id: userId
    }, {
      connected: function connected() {
        //connected stuff
      },
      disconnected: function disconnected() {
        //disconnected stuff
      },
      received: function received(data) {
        var message = this.changeColor(data['message']),
            chat_box = $('[data-conversation-id=' + data['conversation_id'] + ']');
        message_area = chat_box.find('.popup-messages');
        message_area.children('.direct-chat-messages').append(message);
        messages_to_bottom();
        storageHandler.storeConversations();
      },
      send_message: function send_message(message, conversation_id, user_id) {
        this.perform('send_message', { message: message, conversation_id: conversation_id, user_id: user_id });
      },
      changeColor: function changeColor(msg) {
        msg = $(msg);
        var userId = chatContainer.data('current-user'),
            msgAuthorId = msg.data('user-id');

        if (userId == msgAuthorId) {
          msg.find('.direct-chat-text').addClass('author');
        } else {
          msg.find('.direct-chat-text').removeClass('author');
        }
        return msg;
      }
    });
  };

  // Chat box message submit listener
  chatContainer.on('keyup', '.chat_input', function (e) {
    if (e.keyCode == 13) {
      var textBox = $(this),
          conversation = textBox.closest('.popup-box'),
          conversationId = conversation.data('conversation-id'),
          _userId = conversation.data('user-id'),
          message = $.trim(textBox.val());
      if (message.length > 0) {
        App['conversation_' + conversationId].send_message(message, conversationId, _userId);
      }
      textBox.val('');
    }
  })
  // Hide / show chat box
  .on('click', '.hide-chat', function () {
    var button = $(this),
        chatBox = button.closest('.popup-box'),
        glyphIcon = button.find('.glyphicon');
    if (glyphIcon.hasClass('glyphicon-chevron-down')) {
      glyphIcon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
      chatBox.css('bottom', CHAT_HEAD_BODY_DIFFERENCE);
    } else {
      glyphIcon.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
      chatBox.css('bottom', '0px');
    }
    storageHandler.storeConversations();
  })
  // Remove chat
  .on('click', '.remove-chat', function () {
    var chat = $(this).closest('.popup-box'),
        conversationId = chat.data('conversation-id'),
        subscription = 'conversation_' + conversationId;
    purgeSubscription(subscription);
    scrollLoad.removeListener(chat);
    chat.remove();
    arrangeChatBoxes();
    storageHandler.storeConversations();
  });

  // Arrange / Rearrange Chat Boxes
  function arrangeChatBoxes() {
    var chatBoxes = {
      el: null,
      length: null,
      get: function get() {
        this.el = $('.popup-box');
        this.length = this.el.length;
      },
      subscriptionRemove: function subscriptionRemove(chatBox) {
        purgeSubscription('conversation_' + chatBox.data('conversation-id'));
      }
    },
        chatBoxMaxNum = function chatBoxMaxNum() {
      var width = $(window).width();
      return (width - width % CHAT_WIDTH_PLUS_MARGIN) / CHAT_WIDTH_PLUS_MARGIN;
    };
    chatBoxes.get();

    if (chatBoxMaxNum() < chatBoxes.length) {
      for (var i = 0, chatBox = {}; i < chatBoxes.length - chatBoxMaxNum(); i++) {
        chatBox = $(chatBoxes.el[i]);
        chatBoxes.subscriptionRemove(chatBox);
        loadScroll.removeListener(chatBox);
        chatBox.remove();
      }
      chatBoxes.get();
    };

    var rearrange = function rearrange() {
      chatBoxes.get();
      for (var _i = 0, j = chatBoxes.length - 1; _i < chatBoxes.length; _i++, j--) {
        $(chatBoxes.el[j]).css('right', _i * CHAT_WIDTH_PLUS_MARGIN + 70 + 'px');
      }
    };
    rearrange();
  }

  // Purge / Delete Subscriptions
  function purgeSubscription(subscription) {
    App[subscription].unsubscribe();
    delete App[subscription];
  };

  // Load conversations if any stored
  storageHandler.check();

  // NEW MESSAGE
  body.on('click', '.new-personal-message', function (e) {
    userId = $(this).data('user-id');
    chatExists.check();
    console.log('exists? ' + chatExists.exists);
    e.preventDefault();
  }).on('ajax:beforeSend', '.new-personal-message', function (e, xhr, settings) {
    if (chatExists.exists) {
      chatExists.existingObject.find('.chat_input').focus();
      xhr.abort();
    }
  }).on('ajax:success', '.new-personal-message', function (e, data, status, xhr) {
    chatContainer.append(data);
    var conversation = body.find('.popup-box').last();
    message_area = conversation.find('.popup-messages');
    arrangeChatBoxes();
    messages_to_bottom();
    conversationCableHandler(conversation.data('conversation-id'), conversation.data('user-id'));
    storageHandler.storeConversations();
    scrollLoad.addListener(conversation);
  });
});
